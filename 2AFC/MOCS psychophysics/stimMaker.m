function [Stim,Cons] = stimMaker(this,modeToggle)

if modeToggle == 1;
    ConsTemp(1).Description = 'Adapt 45 - Target 24 (177.7)';
    ConsTemp(1).TargetIPD = 0.418879;
    ConsTemp(1).AdaptIPD = 0.785398;
    
    ConsTemp(2).Description = 'Adapt 67.5 - Target 24 (177.7)';
    ConsTemp(2).TargetIPD = 0.418879;
    ConsTemp(2).AdaptIPD = 1.178097;
    
    ConsTemp(3).Description = 'Adapt 90 - Target 24 (177.7)';
    ConsTemp(3).TargetIPD = 0.418879;
    ConsTemp(3).AdaptIPD = 1.5708;
    
    ConsTemp(4).Description = 'Adapt 112.5 - Target 24 (177.7)';
    ConsTemp(4).TargetIPD = 0.418879;
    ConsTemp(4).AdaptIPD = 1.9634954;
    
    ConsTemp(5).Description = 'Adapt 135 - Target 24 (177.7)';
    ConsTemp(5).TargetIPD = 0.418879;
    ConsTemp(5).AdaptIPD = 2.35619;
    
    ConsTemp(6).Description = 'Adapt 157.5 - Target 24 (177.7)';
    ConsTemp(6).TargetIPD = 0.418879;
    ConsTemp(6).AdaptIPD = 2.7488936;
    
    ConsTemp(7).Description = 'Adapt 180 - Target 24 (177.7)';
    ConsTemp(7).TargetIPD = 0.418879;
    ConsTemp(7).AdaptIPD = 3.14159;
    
    ConsTemp(8).Description = 'Adapt 45 - Target -24 (177.7)';
    ConsTemp(8).TargetIPD = -0.418879;
    ConsTemp(8).AdaptIPD = 0.785398;
    
    ConsTemp(9).Description = 'Adapt 67.5 - Target -24 (177.7)';
    ConsTemp(9).TargetIPD = -0.418879;
    ConsTemp(9).AdaptIPD = 1.178097;
    
    ConsTemp(10).Description = 'Adapt 90 - Target -24 (177.7)';
    ConsTemp(10).TargetIPD = -0.418879;
    ConsTemp(10).AdaptIPD = 1.5708;
    
    ConsTemp(11).Description = 'Adapt 112.5 - Target -24 (177.7)';
    ConsTemp(11).TargetIPD = -0.418879;
    ConsTemp(11).AdaptIPD = 1.9634954;
    
    ConsTemp(12).Description = 'Adapt 135 - Target -24 (177.7)';
    ConsTemp(12).TargetIPD = -0.418879;
    ConsTemp(12).AdaptIPD = 2.35619;
    
    ConsTemp(13).Description = 'Adapt 157.5 - Target -24 (177.7)';
    ConsTemp(13).TargetIPD = -0.418879;
    ConsTemp(13).AdaptIPD = 2.7488936;
    
    ConsTemp(14).Description = 'Adapt 180 - Target -24 (177.7)';
    ConsTemp(14).TargetIPD = -0.418879;
    ConsTemp(14).AdaptIPD = 3.14159;
    
    %     ConsTemp(1).Description = 'Adapt 0 - Target 0 (0)';
    %     ConsTemp(1).TargetIPD = 0;
    %     ConsTemp(1).AdaptIPD = 0;
    %
    %     ConsTemp(2).Description = 'Adapt 0 - Target 8 (44.4)';
    %     ConsTemp(2).TargetIPD = 0.139626;
    %     ConsTemp(2).AdaptIPD = 0;
    %
    %     ConsTemp(3).Description = 'Adapt 0 - Target 16 (88.8)';
    %     ConsTemp(3).TargetIPD = 0.279253;
    %     ConsTemp(3).AdaptIPD = 0;
    %
    %     ConsTemp(4).Description = 'Adapt 0 - Target 24 (133.3)';
    %     ConsTemp(4).TargetIPD = 0.418879;
    %     ConsTemp(4).AdaptIPD = 0;
    %
    %     ConsTemp(5).Description = 'Adapt 0 - Target 32 (177.7)';
    %     ConsTemp(5).TargetIPD = 0.558505;
    %     ConsTemp(5).AdaptIPD = 0;
    %
    %     ConsTemp(6).Description = 'Adapt 0 - Target -8 (-44.4)';
    %     ConsTemp(6).TargetIPD = -0.139626;
    %     ConsTemp(6).AdaptIPD = 0;
    %
    %     ConsTemp(7).Description = 'Adapt 0 - Target -16 (-88.8)';
    %     ConsTemp(7).TargetIPD = -0.279253;
    %     ConsTemp(7).AdaptIPD = 0;
    %
    %     ConsTemp(8).Description = 'Adapt 0 - Target -24 (-133.3)';
    %     ConsTemp(8).TargetIPD = -0.418879;
    %     ConsTemp(8).AdaptIPD = 0;
    %
    %     ConsTemp(9).Description = 'Adapt 0 - Target -32 (-177.7)';
    %     ConsTemp(9).TargetIPD = -0.558505;
    %     ConsTemp(9).AdaptIPD = 0;
    %
    %     ConsTemp(10).Description = 'Adapt 90 - Target 0 (0)';
    %     ConsTemp(10).TargetIPD = 0;
    %     ConsTemp(10).AdaptIPD = 1.5708;
    %
    %     ConsTemp(11).Description = 'Adapt 90 - Target 8 (44.4)';
    %     ConsTemp(11).TargetIPD = 0.139626;
    %     ConsTemp(11).AdaptIPD = 1.5708;
    %
    %     ConsTemp(12).Description = 'Adapt 90 - Target 16 (88.8)';
    %     ConsTemp(12).TargetIPD = 0.279253;
    %     ConsTemp(12).AdaptIPD = 1.5708;
    %
    %     ConsTemp(13).Description = 'Adapt 90 - Target 24 (133.3)';
    %     ConsTemp(13).TargetIPD = 0.418879;
    %     ConsTemp(13).AdaptIPD = 1.5708;
    %
    %     ConsTemp(14).Description = 'Adapt 90 - Target 32 (177.7)';
    %     ConsTemp(14).TargetIPD = 0.558505;
    %     ConsTemp(14).AdaptIPD = 1.5708;
    %
    %     ConsTemp(15).Description = 'Adapt 90 - Target -8 (-44.4)';
    %     ConsTemp(15).TargetIPD = -0.139626;
    %     ConsTemp(15).AdaptIPD = 1.5708;
    %
    %     ConsTemp(16).Description = 'Adapt 90 - Target -16 (-88.8)';
    %     ConsTemp(16).TargetIPD = -0.279253;
    %     ConsTemp(16).AdaptIPD = 1.5708;
    %
    %     ConsTemp(17).Description = 'Adapt 90 - Target -24 (-133.3)';
    %     ConsTemp(17).TargetIPD = -0.418879;
    %     ConsTemp(17).AdaptIPD = 1.5708;
    %
    %     ConsTemp(18).Description = 'Adapt 90 - Target -32 (-177.7)';
    %     ConsTemp(18).TargetIPD = -0.558505;
    %     ConsTemp(18).AdaptIPD = 1.5708;
    
    

    
    for i = 1:numel(ConsTemp);
        ConsTemp(i).Coder = i;
    end;
    
    if this.MeasurementData.ConOverwrite == 0;
        Cons = ConsTemp;
    else
        for i = 1:numel(this.MeasurementData.ConOverwrite)
            Cons(i) = ConsTemp(this.MeasurementData.ConOverwrite(i));
        end
    end
    
    
    for i = 1:numel(Cons);
        if this.MeasurementData.ConOverwrite == 0;
            Cons(i).Con_Number = i;
        else
            Cons(i).Con_Number = Cons(i).Coder;
        end
        
    end
    
    this.MeasurementData.Conditions = Cons';
    this.MeasurementData.Conditions = rmfield(this.MeasurementData.Conditions,'Coder');
    
    if  this.MeasurementData.Shuffle == 1; %randomised order
        for i=1:this.MeasurementData.Repeats;
            this.MeasurementData.Runs(i*length(this.MeasurementData.Conditions)-...
                length(this.MeasurementData.Conditions)+1:...
                i*length(this.MeasurementData.Conditions),1)...
                = this.MeasurementData.Conditions(randperm(size(this.MeasurementData.Conditions,1)),:);
        end
    else
        for i=1:this.MeasurementData.Repeats;
            this.MeasurementData.Runs(i*length(this.MeasurementData.Conditions)-...
                length(this.MeasurementData.Conditions)+1:...
                i*length(this.MeasurementData.Conditions),1)...
                = this.MeasurementData.Conditions;
        end
    end
    
    for i=1:length(this.MeasurementData.Runs);
        this.MeasurementData.Runs(i).Trial_Number = i;
        this.MeasurementData.Runs(i).Response =[];
        this.MeasurementData.Runs(i).CodedResponse =[];
    end;
    
    Stim = [];
    return;
end;

switch modeToggle;
    case 2
        Cons = [];
        Stim(:,1) = GenerateStim(...
            'Fs', this.MeasurementData.Fs,...
            'CarrierFrequency', 400,...
            'CarrierPhase', 0,...
            'Amplitude',1);
        Stim(:,2) = GenerateStim(...
            'Fs', this.MeasurementData.Fs,...
            'CarrierFrequency', 400,...
            'CarrierPhase', this.MeasurementData.Runs(this.RunTracker.trialcounter).TargetIPD,...
            'Amplitude',1);
        Adapt(:,1) = GenerateStim(...
            'Fs', this.MeasurementData.Fs,...
            'CarrierFrequency', 400,...
            'Duration', 4,...
            'CarrierPhase', 0,...
            'Amplitude',1);
        Adapt(:,2) = GenerateStim(...
            'Fs', this.MeasurementData.Fs,...
            'CarrierFrequency', 400,...
            'Duration', 4,...
            'CarrierPhase', this.MeasurementData.Runs(this.RunTracker.trialcounter).AdaptIPD,...
            'Amplitude',1);
        sil250 = zeros(2,this.MeasurementData.Fs/4)';
        ref(:,1) = clicks(); ref(:,2) = ref(:,1);
        Stim = vertcat(Adapt,sil250,ref,sil250,Stim);
    otherwise
end


function [value] = GenerateStim(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 0.25); %duration in seconds
s = ef(s, 'Amplitude', 0.2); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'RiseFallTime', 0.1);

time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
cCarrierFrequency = s.CarrierFrequency;
cAmplitude = s.Amplitude;
cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase);
cSignal = cAmplitude.*cCarrier;


cSignal = s.Amplitude*cSignal/max(abs(cSignal)); %normalize sinusoidal to prevent amplitudes higher than 1 when modulating

if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
end

value = cSignal;


function [value, time, s, ncycles] = clicks(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 0.19); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'Phase', 0); % phase in rad
s = ef(s, 'Rate', 16.66); %frequency in Hz
s = ef(s, 'PulseWidth', 0.00050); %pulse width in seconds
s = ef(s, 'Alternating', false); %alternate pulses polarity
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'Description', '');
value = [];
time = [];

cRate = s.Rate;
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
cSignal = zeros(numel(time),1);
ncycles = round(s.Duration*cRate);
cPhase = s.Phase - floor(s.Phase/(2*pi))*2*pi;
for i = 1:ncycles
    cpulse = s.Amplitude*(unitaryStep(time - (1/cRate)*(cPhase)/(2*pi) - ...
        (i-1)/cRate) - unitaryStep(time - (1/cRate)*(cPhase)/(2*pi) - s.PulseWidth - (i-1)/cRate));
    cSignal = cSignal + cpulse*(-1)^((i+1)*s.Alternating);
end

nsamples = numel(cSignal);
value((1-1)*nsamples + 1 : 1*nsamples) = cSignal;


function value = unitaryStep(x)
value = double(x>0);
