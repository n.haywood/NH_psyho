function callbackSelection(hObject, ~)
this = get(hObject,'Userdata');
set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'off');
switch get(hObject, 'Tag')
    case 'pbA'
        this.MeasurementData.Runs(this.RunTracker.trialcounter).Response = 1;
        this.updateFeedBack('');
        endRun(this);
    case 'pbB'
        this.MeasurementData.Runs(this.RunTracker.trialcounter).Response = 2;
        this.updateFeedBack('');
        endRun(this);
        return;
end


end
