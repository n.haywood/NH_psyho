% clear all;
close all; clc;

% Create the GUI window used in the experiment
hfig = figure('units','normalized','outerposition',[0 0 1 1], ...
    'WindowStyle', 'modal',...
    'menubar', 'none',...
    'numbertitle','off');

% Code to make a second window for tracking performance in real time - not
% currently used
% hfEig = figure('units','normalized','outerposition',[0 0 1 0.5], ...
%     'menubar', 'none',...
%     'numbertitle','off');


% Expt parameters go here
hGui = MOCSmodule(...
    'SubjectName','NH',...
    'Expt','Pilot_latshift_A',...
    'SaveFolder','C:\Users\jundurraga\Desktop\MOCS psychophysics\Data\',...
    'Repeats',20,...
    'Feedback',0,... %Toggle correct/incorrect feedback (1 = on, 0 = off)
    'Shuffle',1,...
    'HContainer', hfig,...
    'Fs',44100 ...
);