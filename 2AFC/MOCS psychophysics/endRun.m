function endRun(this)

this.MeasurementData.Runs(this.RunTracker.trialcounter).CompletedFlag = 1;
cStructholder.results = this.MeasurementData;
cStructholder.results  = rmfield(cStructholder.results,'Conditions');
cStructholder.results  = rmfield(cStructholder.results,'Runs');
for i = 1:numel(this.MeasurementData.Runs)
cStructholder.results.Runs{i} = this.MeasurementData.Runs(i);
end;
struct2xml(cStructholder,this.MeasurementData.Outfile);


cStructholder.results = this.MeasurementData;
cStructholder.results  = rmfield(cStructholder.results,'Conditions');
% following two lines make it convertible to xml file by making each line
% in the table to a cell
for i = 1:numel(this.MeasurementData.Runs)
cStructholder.results.Run{i} = this.MeasurementData.Runs(i);
end;
cStructholder.results  = rmfield(cStructholder.results,'Runs');
struct2xml(cStructholder,this.MeasurementData.Outfile);

if this.RunTracker.trialcounter == numel(this.MeasurementData.Runs)
    msgbox('The experiment is over, thank you for your time!!')
else
    startRun(this);
    
end
end

