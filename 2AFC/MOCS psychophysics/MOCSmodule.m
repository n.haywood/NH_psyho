classdef MOCSmodule < handle
    properties
        HContainer = []; %handles of object containing visual objects
        MeasurementData = [];
        RunTracker = [];
    end
    
    events
    end
    
    properties (GetAccess = private)
    end
    
    methods
        %%
        function this = MOCSmodule(varargin)
            s = parseparameters(varargin{:});
            s = ef(s,'HContainer', []);%handle to container object
            s = ef(s,'SubjectName',[]);
            s = ef(s,'Expt',[]);
            s = ef(s,'Repeats',[]);
            s = ef(s,'Shuffle',[]);
            s = ef(s,'Feedback',0);
            s = ef(s,'Date',datestr(now, 'dd/mmm/yyyy'));
            s = ef(s,'SaveFolder',[]);
            s = ef(s,'inputstate',1);
            s = ef(s,'Fs',[]);
            s = ef(s,'ConOverwrite',0);
            
            %% Some basic transfers from varargin into MeasurementData structure
            this.MeasurementData.Fs = s.Fs;
            this.MeasurementData.SubjectName = s.SubjectName;
            this.MeasurementData.Expt = s.Expt;
            this.MeasurementData.Repeats = s.Repeats;
            this.MeasurementData.Shuffle = s.Shuffle;
            this.MeasurementData.ConOverwrite = s.ConOverwrite;
            this.MeasurementData.Feedback = s.Feedback;
            this.HContainer = s.HContainer;
            set(this.HContainer, 'KeyPressFcn', @this.keyPress);
            this.RunTracker.inputstate = s.inputstate;
            this.RunTracker.trialcounter = 0;
            
%            InitializePsychSound;
  %          PsychPortAudio('Close'); 
    %        this.RunTracker.pahandle = PsychPortAudio('Open' ,3,1,0,this.MeasurementData.Fs,2);
            
            %% Create Output file, and check if already exists (option to exit or overwrite)
            this.MeasurementData.Outfile = ...
                horzcat(s.SaveFolder,this.MeasurementData.Expt,'-',this.MeasurementData.SubjectName,'.xml');
            if exist(this.MeasurementData.Outfile,'file') == 2;
                choice = questdlg(...
                    'The save file already exisits, would you like to:', ...
                    'Warning!!', ...
                    'Exit','Overwrite','Overwrite');
                
                switch choice
                    case 'Exit'
                        close all;
                        return;
                    case 'Overwrite'
                end
            end;
            
            
            %% create run list
            [~,this.MeasurementData.Conditions] = stimMaker(this,1);
            this.setPsychoModuleGui;
            cStructholder.results = this.MeasurementData;
            cStructholder.results  = rmfield(cStructholder.results,'Conditions');
            % following two lines make it convertible to xml file by making each line
            % in the table to a cell
            for i = 1:numel(this.MeasurementData.Runs)
                cStructholder.results.Run{i} = this.MeasurementData.Runs(i);
            end;
            cStructholder.results  = rmfield(cStructholder.results,'Runs');
%            struct2xml(cStructholder,this.MeasurementData.Outfile);
            startRun(this)
        end
        
        %%
        function setPsychoModuleGui(this)
            setPsychoModuleGUI(this);
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'off');
        end
        
        %% Take keyboard inputs
        function keyPress(this, ~, event)
            switch event.Key
                case 'space'
%                     if this.RunTracker.inputstate == 1;
%                         set(this.HContainer, 'KeyPressFcn', '');
%                         this.RunTracker.inputstate = 2;
%                         doRun(this)
%                     end
                    
                case 'a'
                    if this.RunTracker.inputstate == 2;
                        set(this.HContainer, 'KeyPressFcn', '');
                        pause(0.1)
                        hbA = findobj(this.HContainer, 'Tag', 'pbA');
                        hgfeval(get(hbA ,'Callback'),hbA);
                    end
                case 'd'
                    if this.RunTracker.inputstate == 2;
                        set(this.HContainer, 'KeyPressFcn', '');
                        pause(0.1)
                        hbB = findobj(this.HContainer, 'Tag', 'pbB');
                        hgfeval(get(hbB ,'Callback'), hbB);
                    end
                otherwise
            end
        end
        
        %%
        function doRun(this, ~, ~)
            [cInterval,~] = stimMaker(this,2);
            pause(1);
            this.updateFeedBack('Listen');
            PsychPortAudio('FillBuffer', this.RunTracker.pahandle, cInterval');
            PsychPortAudio('Start', this.RunTracker.pahandle,1,0,1)
            pause(length(cInterval)*1/this.MeasurementData.Fs);
            this.updateFeedBack('');
            pause(0.2);
            
            this.updateFeedBack('Left or Right?');
            set(this.HContainer, 'KeyPressFcn', @this.keyPress);
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'on');
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'on');
        end
        
        %%
        function updateFeedBack(this, message)
            set(findobj(this.HContainer, 'Tag', 'txFeedBack'), 'String', message);
        end
        
        function updateExptFeedBack(this, trialNum,trialDescription)
            set(findobj(this.HEContainer, 'Tag', 'trialNum'), 'String', trialNum);
            set(findobj(this.HEContainer, 'Tag', 'trialDescription'), 'String', trialDescription);
            set(findobj(this.HEContainer, 'Tag', 'runCounter'), 'String',...
                ['Number of Responses: ' num2str(numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses))]);
        end
        
    end
    
end
