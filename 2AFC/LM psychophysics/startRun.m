function startRun(this)

this.RunTracker.trialcounter = this.RunTracker.trialcounter+1;


if this.RunTracker.trialcounter == 1;
    this.updateFeedBack(['Welcome! Press start to begin run ' num2str(this.RunTracker.trialcounter)]);
end

if this.RunTracker.trialcounter > 1;
    this.updateFeedBack(['Run complete! Press start to begin run ' num2str(this.RunTracker.trialcounter)]);
end;


this.RunTracker.Step = 9; % THIS IS THE STARTING STEP _ CAN BE CHANGED
this.RunTracker.runcounter = 0;

this.MeasurementData.Runs(this.RunTracker.trialcounter).StimTracker(1,1) = 1;
set(findobj(this.HContainer, 'Tag', 'startRun'), 'Visible', 'on');
%figure(this.HEContainer);

%this.RunTracker.plotter  = this.MeasurementData.Runs(this.RunTracker.trialcounter).StimTracker(1,:)
%subplot(1,2,2); plot(1);

%axis([0,1,...
%    min(this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues)-1,...
%    max(this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues)+1]);


%this.RunTracker.inputstate = 1;
%set(this.HContainer, 'KeyPressFcn', @this.keyPress);
%updateExptGui(this,1,this.MeasurementData.ExptPanelToggle);
%[this.RunTracker.RefInterval,~,~] = stimMaker(this,2);



end

