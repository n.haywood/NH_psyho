function [ output_args ] = callbackExitCheck(hObject, ~)
this = get(hObject,'Userdata');
set(findobj(this.HContainer, 'Tag', 'endrunyes'), 'Visible', 'on');
set(findobj(this.HContainer, 'Tag', 'endrunno'), 'Visible', 'on');
set(findobj(this.HContainer, 'Tag', 'vol--'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'vol-'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'vol='), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'vol+'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'vol++'), 'Enable', 'off');
this.updateFeedBack('Press ''Yes'' to confirm');
%CALLBACKEXITCHECK Summary of this function goes here
%   Detailed explanation goes here


end

