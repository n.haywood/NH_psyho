% clear all;
close all; clc;

% Create the GUI window used in the experiment
hfig = figure('units','normalized','outerposition',[0 0 1 1], ...
    'WindowStyle', 'modal',...
    'menubar', 'none',...
    'numbertitle','off');

% Code to make a second window for tracking performance in real time - not
% currently used
% hfEig = figure('units','normalized','outerposition',[0 0 1 0.5], ...
%     'menubar', 'none',...
%     'numbertitle','off');


% Expt parameters go here
hGui = PsychoModuleLM(...
    'SubjectName','DB',...
    'Expt','Pilot',...
    'Repeats',2,...
    'Shuffle',1,...
    'SaveFolder','C:\Users\jundurraga\Desktop\LM psychophysics\Data\',...
    'HContainer', hfig,...
    'ConOverwrite',[1 2 3],... 
    'Fs',48000 ...
);

% Training 1:'ConOverwrite',[1 1 1 1 1 0 0 0 0 ]...
% Training 2:'ConOverwrite',[1 1 1 1 1 0 0 0 0 ]...
% 
%     1 = 500;
%     2 = 2000;
%     3 = 4000;
