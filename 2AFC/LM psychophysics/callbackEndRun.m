function [ output_args ] = callbackEndRun(hObject, ~)
this = get(hObject,'Userdata');
endRun(this)
set(findobj(this.HContainer, 'Tag', 'endrunyes'), 'Visible', 'off');
set(findobj(this.HContainer, 'Tag', 'endrunno'), 'Visible', 'off');
end

