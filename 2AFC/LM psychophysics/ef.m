function s = ef(s, fieldName, defaultVal)
if ~isfield(s, fieldName)
    s = setfield(s, fieldName,defaultVal);
end