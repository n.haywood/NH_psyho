function callbackSelection(hObject, ~)
this = get(hObject,'Userdata');
            set(findobj(this.HContainer, 'Tag', 'vol--'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol-'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol='), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol+'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol++'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'off');
this.MeasurementData.Runs(this.RunTracker.trialcounter).StepAtResponse(this.RunTracker.runcounter)...
    = this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues(this.RunTracker.Step);
switch get(hObject, 'Tag')
    case 'vol++'
        this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses(this.RunTracker.runcounter) = 3; %1 indicates a 'make loder' response
        this.updateFeedBack('Make Louder');
        %set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.7 1 0.7]);
        
        if this.RunTracker.Step < (numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues)-2);
            this.RunTracker.Step = this.RunTracker.Step + 3; %big step
        else;
            if this.RunTracker.Step == numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues);
                this.updateFeedBack('This is as loud as it gets!');
            end
            this.RunTracker.Step = numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues);
        end;
 
            case 'vol+'
        this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses(this.RunTracker.runcounter) = 1; %1 indicates a 'make loder' response
        this.updateFeedBack('Make Louder');
        %set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.7 1 0.7]);
        
        if this.RunTracker.Step < numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues);
            this.RunTracker.Step = this.RunTracker.Step + 1; %small step
        else;
            this.updateFeedBack('This is as loud as it gets!');
        end;
        
        
    case 'vol--'
        this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses(this.RunTracker.runcounter) = -3; %0 indicates a 'make quieter' response
        this.updateFeedBack('Make Quieter');
        
        if this.RunTracker.Step <4;
            if this.RunTracker.Step == 1;
                this.updateFeedBack('This is as quiet as it gets!');
            end   
            this.RunTracker.Step = 1;
        else;
            this.RunTracker.Step = this.RunTracker.Step - 3; %big step
        end;
        
            case 'vol-'
        this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses(this.RunTracker.runcounter) = -1; %0 indicates a 'make quieter' response
        this.updateFeedBack('Make Quieter');
        
        if this.RunTracker.Step == 1;
            this.updateFeedBack('This is as quiet as it gets!');
        else;
            this.RunTracker.Step = this.RunTracker.Step - 1; %small step
        end;  
end

cStructholder.results = this.MeasurementData;
cStructholder.results  = rmfield(cStructholder.results,'Conditions');
cStructholder.results  = rmfield(cStructholder.results,'Runs');
for i = 1:numel(this.MeasurementData.Runs)
cStructholder.results.Runs{i} = this.MeasurementData.Runs(i);
end;
struct2xml(cStructholder,this.MeasurementData.Outfile);

doRun(this);
end
