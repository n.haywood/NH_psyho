function endRun(this)
this.MeasurementData.Runs(this.RunTracker.trialcounter).StepAtResponse(this.RunTracker.runcounter)...
    = this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues(this.RunTracker.Step);

this.MeasurementData.Runs(this.RunTracker.trialcounter).CompletedFlag = true;
cStructholder.results = this.MeasurementData;
cStructholder.results  = rmfield(cStructholder.results,'Conditions');
cStructholder.results  = rmfield(cStructholder.results,'Runs');
for i = 1:numel(this.MeasurementData.Runs)
cStructholder.results.Runs{i} = this.MeasurementData.Runs(i);
end;
struct2xml(cStructholder,this.MeasurementData.Outfile);

if this.RunTracker.trialcounter == numel(this.MeasurementData.Runs)
    msgbox('The experiment is over, thank you for your time!!')
else
    startRun(this);
    
end

