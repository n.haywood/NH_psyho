function [ output_args ] = callbackReturn(hObject, ~)
this = get(hObject,'Userdata');
this.updateFeedBack('Adjust the volume of the second sound to match the first sound');
set(findobj(this.HContainer, 'Tag', 'endrunyes'), 'Visible', 'off');
set(findobj(this.HContainer, 'Tag', 'endrunno'), 'Visible', 'off');
set(findobj(this.HContainer, 'Tag', 'vol--'), 'Enable', 'on');
set(findobj(this.HContainer, 'Tag', 'vol-'), 'Enable', 'on');
set(findobj(this.HContainer, 'Tag', 'vol='), 'Enable', 'on');
set(findobj(this.HContainer, 'Tag', 'vol+'), 'Enable', 'on');
set(findobj(this.HContainer, 'Tag', 'vol++'), 'Enable', 'on');

end

