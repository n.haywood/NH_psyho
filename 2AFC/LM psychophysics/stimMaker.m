function [ReferenceStim, TargetStim,  Cons] = stimMaker(this,modeToggle)

if modeToggle == 1;
    ConsTemp(1).Description = '500Hz';
    ConsTemp(2).Description = '2000Hz';
    ConsTemp(3).Description = '4000Hz';
    
    for i = 1:numel(ConsTemp);
        ConsTemp(i).Coder = i;
    end;
    
    if this.MeasurementData.ConOverwrite == 0;
        Cons = ConsTemp;
    else
        for i = 1:numel(this.MeasurementData.ConOverwrite)
            Cons(i) = ConsTemp(this.MeasurementData.ConOverwrite(i));
        end
    end
    
    
    for i = 1:numel(Cons);
        if this.MeasurementData.ConOverwrite == 0;
            Cons(i).Con_Number = i;
        else
            Cons(i).Con_Number = Cons(i).Coder;
        end
        
        Cons(i).Responses = [];
        Cons(i).StepValues = [-24:2:16]; %HERE ARE THE STEP VALUES IN DB
    end
    
    ReferenceStim = [];
    TargetStim = [];
    return;
end;

% this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues(this.RunTracker.Step)
switch this.MeasurementData.Runs(this.RunTracker.trialcounter).Con_Number
    case 1
        cTarFrequency = 500;
        cAMP = 0.0628;
    case 2
        cTarFrequency = 2000;
        cAMP = 0.0729;
    case 3
        cTarFrequency = 4000;
        cAMP = 0.0940;
    otherwise
        return;
end


switch modeToggle;
    case 2
        Cons = [];
        
        [ReferenceStim(:,1)] = GenerateStim(...
            'CarrierFrequency',1000,...
            'Amplitude', 0.025,...
            'Fs', this.MeasurementData.Fs);
        
        [TargetStim(:,1)] = GenerateStim(...
            'CarrierFrequency',cTarFrequency,...
            'Amplitude', 10^(this.MeasurementData.Runs(this.RunTracker.trialcounter).StepValues(this.RunTracker.Step)/20)*cAMP ,...
            'Fs', this.MeasurementData.Fs);
        
        
        
    otherwise
        Cons = [];
        TargetStim = [];
        ReferenceStim = [];
end

%%
function [value] = GenerateStim(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 0.1); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', true);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0.1);
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters;
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency = s.CarrierFrequency;
cModulationFrequency = s.ModulationFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit carrier freq
    cCarrierFrequency  = round(s.Duration*cCarrierFrequency)/s.Duration;
    s.CarrierFrequency  = cCarrierFrequency;
    %fit modulation rate
    cModulationFrequency   = round(s.Duration*cModulationFrequency)/s.Duration;
    s.ModulationFrequency  = cModulationFrequency;
    % compute intermodulation frequency (phantom beating) as output only
    [~,D1] = rat(cModulationFrequency/cCarrierFrequency, 1e-12);
    s.IntermodulatedFrequency = cCarrierFrequency/D1;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase);
cSignal = cAmplitude.*cCarrier;
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignal = s.Amplitude*cSignal/max(abs(cSignal));
nsamples = numel(cSignal);
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
end

value = zeros(nsamples*s.NRepetitions,1);
%progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples) = factor*cSignal;
end
