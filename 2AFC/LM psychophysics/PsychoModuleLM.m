classdef PsychoModuleLM < handle
    properties
        HContainer = []; %handles of object containing visual objects
        MeasurementData = [];
        RunTracker = [];
    end
    
    events
    end
    
    properties (GetAccess = private)
    end
    
    methods
        %%
        function this = PsychoModuleLM(varargin)
            s = parseparameters(varargin{:});
            s = ef(s,'HContainer', []);%handle to container object
            s = ef(s,'SubjectName',[]);
            s = ef(s,'Expt',[]);
            s = ef(s,'Repeats',[]);
            s = ef(s,'Date',datestr(now, 'dd/mmm/yyyy'));
            s = ef(s,'SaveFolder',[]);
            s = ef(s,'Shuffle',[]);
            s = ef(s,'inputstate',1);
            s = ef(s,'Fs',[]);
            s = ef(s,'ShowThres',[]);
            s = ef(s,'ConOverwrite',0);
            
            %% Some basic transfers from varargin into MeasurementData structure
            this.MeasurementData.Fs = s.Fs;
            this.MeasurementData.SubjectName = s.SubjectName;
            this.MeasurementData.Expt = s.Expt;
            this.MeasurementData.Repeats = s.Repeats;
            this.MeasurementData.Shuffle = s.Shuffle;
            this.MeasurementData.ShowThres = s.ShowThres;
            this.MeasurementData.ConOverwrite = s.ConOverwrite;
            
            this.HContainer = s.HContainer;
            set(this.HContainer, 'KeyPressFcn', @this.keyPress);
            this.RunTracker.inputstate = s.inputstate;
            this.RunTracker.trialcounter = 0;
            % PsychPortAudio('GetDevices') - list all devices!
            
            InitializePsychSound;
            PsychPortAudio('Close')
            this.RunTracker.pahandle = PsychPortAudio('Open' ,17,1,0,this.MeasurementData.Fs,1);
            % this specifies soundcard output
            
            %[~,~,this.MeasurementData.Conditions] = stimMaker(this,1);
            
            %% Create Output file, and check if already exists (option to exit or overwrite)
            %Should add append option in future!!
            this.MeasurementData.Outfile = ...
                horzcat(s.SaveFolder,this.MeasurementData.Expt,'-',this.MeasurementData.SubjectName,'.xml');
            if exist(this.MeasurementData.Outfile,'file') == 2;
                choice = questdlg(...
                    'The save file already exisits, would you like to:', ...
                    'Warning!!', ...
                    'Exit','Overwrite','Overwrite');
                
                switch choice
                    case 'Exit'
                        close all;
                        return;
                    case 'Overwrite'
                end
            end;
            
            %% run list
            [~,~,this.MeasurementData.Conditions] = stimMaker(this,1);
            this.MeasurementData.Conditions = this.MeasurementData.Conditions';
            
            if  this.MeasurementData.Shuffle == 1; %randomised order
                for i=1:this.MeasurementData.Repeats;
                    this.MeasurementData.Runs(i*length(this.MeasurementData.Conditions)-...
                        length(this.MeasurementData.Conditions)+1:...
                        i*length(this.MeasurementData.Conditions),1)...
                        = this.MeasurementData.Conditions(randperm(size(this.MeasurementData.Conditions,1)),:);
                end
            else
                for i=1:this.MeasurementData.Repeats;
                    this.MeasurementData.Runs(i*length(this.MeasurementData.Conditions)-...
                        length(this.MeasurementData.Conditions)+1:...
                        i*length(this.MeasurementData.Conditions),1)...
                        = this.MeasurementData.Conditions;
                end
            end
            
            for i=1:length(this.MeasurementData.Runs);
                this.MeasurementData.Runs(i).Trial_Number = i;
                this.MeasurementData.Runs(i).CodedResponses =[];
            end;
            
            cStructholder.results = this.MeasurementData;
            cStructholder.results  = rmfield(cStructholder.results,'Conditions');
            cStructholder.results  = rmfield(cStructholder.results,'Runs');
            for i = 1:numel(this.MeasurementData.Runs)
                cStructholder.results.Runs{i} = this.MeasurementData.Runs(i);
            end;
            struct2xml(cStructholder,this.MeasurementData.Outfile);
            
            this.setPsychoModuleGui;
            
            %setPsychoModuleExptGUI(this);
            %updateExptGui(this,0,this.MeasurementData.ExptPanelToggle)
            startRun(this)
        end
        
        %%
        function setPsychoModuleGui(this)
            setPsychoModuleGUI(this);
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol--'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol-'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol='), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol+'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'vol++'), 'Enable', 'off');
            
        end
        
        %% Take keyboard inputs
        function keyPress(this, ~, event)
            switch event.Key
                case 'space'
                    if this.RunTracker.inputstate == 1;
                        set(this.HContainer, 'KeyPressFcn', '');
                        this.RunTracker.inputstate = 2;
                        doRun(this)
                    end
                    
                case 'a'
                    if this.RunTracker.inputstate == 2;
                        set(this.HContainer, 'KeyPressFcn', '');
                        set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.4 0.4 0.4]);
                        pause(0.1)
                        hbA = findobj(this.HContainer, 'Tag', 'pbA');
                        hgfeval(get(hbA ,'Callback'),hbA);
                    end
                    
                case 'd'
                    if this.RunTracker.inputstate == 2;
                        set(this.HContainer, 'KeyPressFcn', '');
                        set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [0.4 0.4 0.4]);
                        pause(0.1)
                        hbB = findobj(this.HContainer, 'Tag', 'pbB');
                        hgfeval(get(hbB ,'Callback'), hbB);
                    end
                    
                case 'm'
                    if this.RunTracker.inputstate == 3;
                        endRun(this)
                    end
                    if this.RunTracker.inputstate == 2;
                        this.RunTracker.inputstate = 3;
                        this.updateFeedBack('Press m again to confirm, press n to return');
                    end
                    
                    
                case 'n'
                    if this.RunTracker.inputstate == 3;
                        this.RunTracker.inputstate = 2;
                        this.updateFeedBack('Adjust the volume of the second sound to make it equal to the first');
                    end
                    
                otherwise
            end
        end
        
        %%
        function doRun(this, ~, ~)
            this.RunTracker.runcounter = this.RunTracker.runcounter +1;
            [this.RunTracker.RefInterval,this.RunTracker.TargetInterval,~] = stimMaker(this,2);
            
            pause(1.5); %pause between response press and next playback
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.8 0.8 0.8]);
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [0.8 0.8 0.8]);
            
            %if rand() > 0.5;    % Randomize which of the two intervals
            %contains the target ('correct') stimuli - randomization
            %commented out from loudness matching procedure
            [FirstInterval] = this.RunTracker.RefInterval;
            [SecondInterval] = this.RunTracker.TargetInterval;
            %else
            %[FirstInterval] = this.RunTracker.TargetInterval;
            %[SecondInterval] = this.RunTracker.RefInterval;
            % end;
            
            
            this.updateFeedBack('First Sound');
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.7 0.7 1]);
            PsychPortAudio('FillBuffer', this.RunTracker.pahandle, FirstInterval');
            PsychPortAudio('Start', this.RunTracker.pahandle,1,0,1)
            pause(length(FirstInterval)*1/this.MeasurementData.Fs);
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.8 0.8 0.8]);
            this.updateFeedBack('');
            pause(1);%pause between playback from interval 1 & 2
            
            this.updateFeedBack('Second Sound');
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [0.7 0.7 1]);
            PsychPortAudio('FillBuffer', this.RunTracker.pahandle, SecondInterval');
            PsychPortAudio('Start', this.RunTracker.pahandle,1,0,1)
            pause(length(SecondInterval)*1/this.MeasurementData.Fs);
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [0.8 0.8 0.8]);
            this.updateFeedBack('');
            pause(1);
            
            this.updateFeedBack('Adjust the volume of the second sound to match the first sound');
            set(this.HContainer, 'KeyPressFcn', @this.keyPress);
            set(findobj(this.HContainer, 'Tag', 'vol--'), 'Enable', 'on');
            set(findobj(this.HContainer, 'Tag', 'vol-'), 'Enable', 'on');
            set(findobj(this.HContainer, 'Tag', 'vol='), 'Enable', 'on');
            set(findobj(this.HContainer, 'Tag', 'vol+'), 'Enable', 'on');
            set(findobj(this.HContainer, 'Tag', 'vol++'), 'Enable', 'on');
        end
        
        %%
        function updateFeedBack(this, message)
            set(findobj(this.HContainer, 'Tag', 'txFeedBack'), 'String', message);
        end
        
        function updateExptFeedBack(this, trialNum,trialDescription)
            set(findobj(this.HEContainer, 'Tag', 'trialNum'), 'String', trialNum);
            set(findobj(this.HEContainer, 'Tag', 'trialDescription'), 'String', trialDescription);
            set(findobj(this.HEContainer, 'Tag', 'runCounter'), 'String',...
                ['Number of Responses: ' num2str(numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses))]);
        end
        
    end
    
end
