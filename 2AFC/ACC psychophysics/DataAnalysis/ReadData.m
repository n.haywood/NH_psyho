%[filename, pathname] = uigetfile('*.xml','Select the results file','C:\myresultsdirectory');
%- as below, except defult directory is specified
clear all

[filename, pathname] = uigetfile('*.xml','Select the results file');

if isequal(filename, 0)
    return;
end

cImportedParams = convertxmlvalue2matvalue(xml2struct(strcat(pathname,filename)));
Results = cImportedParams.results;
clear cImportedParams filename pathname


for CondtionCounter = 1:6
    TempCounter = 1;
    Condtion{CondtionCounter}.NumberCorrect = 0;
    Condtion{CondtionCounter}.NumberAnswered = 0;
    for TrialCounter = 1:numel(Results.Run);
        if Results.Run{TrialCounter}.Con_Number == CondtionCounter;
            if TempCounter == 1;
                Condtion{CondtionCounter}.ConName = Results.Run{TrialCounter}.Description;
            end;
            
            Con{CondtionCounter,TempCounter} = Results.Run{TrialCounter};
            if isnumeric(Results.Run{TrialCounter}.CodedResponse) == 1;
                Condtion{CondtionCounter}.NumberCorrect = Condtion{CondtionCounter}.NumberCorrect+ Results.Run{TrialCounter}.CodedResponse;
                Condtion{CondtionCounter}.NumberAnswered = Condtion{CondtionCounter}.NumberAnswered +1;
            end
            TempCounter = TempCounter+1;
        end
    end
end
