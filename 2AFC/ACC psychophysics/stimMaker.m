function [Stim1, Stim2,  Cons] = stimMaker(this,modeToggle)

if modeToggle == 1;
    
    %SET TO LOUDNESS MATCHED VALUES (IN RELATIVE DB: EG: -2, 0 +2, etc...)
    amp500 = 3;
    amp2000 = -10;
    amp4000 = -10;
    %
    
    amp500 = 10^(amp500/20)*0.0628;
    amp1000 = 0.025;
    amp2000 = 10^(amp2000/20)*0.0729;
    amp4000 = 10^(amp4000/20)*0.0940;
    
    ConsTemp(1).Description = '500 Hz - 1000 Hz';
    ConsTemp(1).Seq1 = 500;
    ConsTemp(1).Seq2 = 1000;
    ConsTemp(1).Seq1Amp = amp500;
    ConsTemp(1).Seq2Amp = amp1000;
    ConsTemp(1).Correct = 2;
    
    ConsTemp(2).Description = '2000 Hz - 1000 Hz';
    ConsTemp(2).Seq1 = 2000;
    ConsTemp(2).Seq2 = 1000;
    ConsTemp(2).Seq1Amp = amp2000;
    ConsTemp(2).Seq2Amp = amp1000;
    ConsTemp(2).Correct = 1;
    
    ConsTemp(3).Description = '4000 Hz - 1000 Hz';
    ConsTemp(3).Seq1 = 4000;
    ConsTemp(3).Seq2 = 1000;
    ConsTemp(3).Seq1Amp = amp4000;
    ConsTemp(3).Seq2Amp = amp1000;
    ConsTemp(3).Correct = 1;
    
    ConsTemp(4).Description = '2000 Hz - 500 Hz';
    ConsTemp(4).Seq1 = 2000;
    ConsTemp(4).Seq2 = 500;
    ConsTemp(4).Seq1Amp = amp2000;
    ConsTemp(4).Seq2Amp = amp500;
    ConsTemp(4).Correct = 1;
    
    ConsTemp(5).Description = '4000 Hz - 500 Hz';
    ConsTemp(5).Seq1 = 4000;
    ConsTemp(5).Seq2 = 500;
    ConsTemp(5).Seq1Amp = amp4000;
    ConsTemp(5).Seq2Amp = amp500;
    ConsTemp(5).Correct = 1;
    
    ConsTemp(6).Description = '4000 Hz - 2000 Hz';
    ConsTemp(6).Seq1 = 4000;
    ConsTemp(6).Seq2 = 2000;
    ConsTemp(6).Seq1Amp = amp4000;
    ConsTemp(6).Seq2Amp = amp2000;
    ConsTemp(6).Correct = 1;
    
    for i = 1:numel(ConsTemp);
        ConsTemp(i).Coder = i;
    end;
    
    if this.MeasurementData.ConOverwrite == 0;
        Cons = ConsTemp;
    else
        for i = 1:numel(this.MeasurementData.ConOverwrite)
            Cons(i) = ConsTemp(this.MeasurementData.ConOverwrite(i));
        end
    end
    
    
    for i = 1:numel(Cons);
        if this.MeasurementData.ConOverwrite == 0;
            Cons(i).Con_Number = i;
        else
            Cons(i).Con_Number = Cons(i).Coder;
        end
        
    end
    
    this.MeasurementData.Conditions = Cons';
    this.MeasurementData.Conditions = rmfield(this.MeasurementData.Conditions,'Coder');
    
    if  this.MeasurementData.Shuffle == 1; %randomised order
        for i=1:this.MeasurementData.Repeats;
            this.MeasurementData.Runs(i*length(this.MeasurementData.Conditions)-...
                length(this.MeasurementData.Conditions)+1:...
                i*length(this.MeasurementData.Conditions),1)...
                = this.MeasurementData.Conditions(randperm(size(this.MeasurementData.Conditions,1)),:);
        end
    else
        for i=1:this.MeasurementData.Repeats;
            this.MeasurementData.Runs(i*length(this.MeasurementData.Conditions)-...
                length(this.MeasurementData.Conditions)+1:...
                i*length(this.MeasurementData.Conditions),1)...
                = this.MeasurementData.Conditions;
        end
    end
    
    for i=1:length(this.MeasurementData.Runs);
        this.MeasurementData.Runs(i).Trial_Number = i;
        this.MeasurementData.Runs(i).Response =[];
        this.MeasurementData.Runs(i).CodedResponse =[];
    end;
    
    Stim1 = [];
    Stim2 = [];
    return;
end;

switch modeToggle;
    case 2
        Cons = [];
        
        Stim1(:,1) = GenerateStim(...
            'CarrierFrequency', this.MeasurementData.Runs(this.RunTracker.trialcounter).Seq1,...
            'Amplitude', this.MeasurementData.Runs(this.RunTracker.trialcounter).Seq1Amp);
        
        Stim2(:,1) = GenerateStim(...
            'CarrierFrequency', this.MeasurementData.Runs(this.RunTracker.trialcounter).Seq2,...
            'Amplitude', this.MeasurementData.Runs(this.RunTracker.trialcounter).Seq2Amp);
        
    otherwise
        Cons = [];
        TargetStim = [];
        ReferenceStim = [];
end


function [value] = GenerateStim(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'NRepetitions', 1); %sampling frequency
s = ef(s, 'Amplitude', 0.2); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'ModulationFrequency', 0); %frequency in Hz
s = ef(s, 'ModulationPhase', 0); %frequency in Hz
s = ef(s, 'ModulationIndex', 0); %frequency in Hz
s = ef(s, 'OnlyReturnParameters', 0); % dummy var
s = ef(s, 'RoundToCycle', false);% round frequencies to fit cycles in time
s = ef(s, 'CycleRate', s.Fs); % this is the sampling rate to be matched with Fs. For example, match Fs from sound card and recording device
s = ef(s, 'AlternateRepetitions', false);% if true, the polarity of the repetition will be alternated
s = ef(s, 'RiseFallTime', 0.1);
s = ef(s, 'Description', '');
value = [];
time = [];

if s.OnlyReturnParameters;
    % this is used to return only the structure
    s = rmfield(s,'OnlyReturnParameters');
    return;
end;
s = rmfield(s,'OnlyReturnParameters');

cCarrierFrequency = s.CarrierFrequency;
cModulationFrequency = s.ModulationFrequency;
if s.RoundToCycle
    %% compute least common multiple between fs and CycleRate
    [N1, ~] = rat(s.CycleRate/s.Fs, 1e-12);
    cMinDuration = N1/s.CycleRate;
    % now we change the input duration to the closet minduration multiple
    s.Duration = ceil(s.Duration/cMinDuration)*cMinDuration;
    %fit carrier freq
    cCarrierFrequency  = round(s.Duration*cCarrierFrequency)/s.Duration;
    s.CarrierFrequency  = cCarrierFrequency;
    %fit modulation rate
    cModulationFrequency   = round(s.Duration*cModulationFrequency)/s.Duration;
    s.ModulationFrequency  = cModulationFrequency;
    % compute intermodulation frequency (phantom beating) as output only
    [~,D1] = rat(cModulationFrequency/cCarrierFrequency, 1e-12);
    s.IntermodulatedFrequency = cCarrierFrequency/D1;
end
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;

cAmplitude = s.Amplitude*(1 - s.ModulationIndex*cos(2*pi*cModulationFrequency*time + ...
    s.ModulationPhase));
cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase);
cSignal = cAmplitude.*cCarrier;
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignal = s.Amplitude*cSignal/max(abs(cSignal));
nsamples = numel(cSignal);
%% generates rise fall window
if s.RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.Duration, ...
        'RiseFallTime', s.RiseFallTime);
    cSignal = cWindow.*cSignal;
end

value = zeros(nsamples*s.NRepetitions,1);
%progressbar('Generating signal');
for i = 1:s.NRepetitions
    progressbar(i/s.NRepetitions);
    factor = 1;
    if s.AlternateRepetitions
        factor = (-1)^(i+1);
    end
    value((i-1)*nsamples + 1 : i*nsamples) = factor*cSignal;
end


