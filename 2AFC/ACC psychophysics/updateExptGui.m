function updateExptGui(this,modetoggle,exist)
if exist == 0;
    return;
end;

switch modetoggle;
    case 0
        
        return;
        
    case 1
        this.updateExptFeedBack(['Trial Number: ' num2str(this.RunTracker.trialcounter)],['Description: ' this.MeasurementData.Runs(this.RunTracker.trialcounter).Description])
        
        
        return;
        
    case 2
        set(findobj(this.HEContainer, 'Tag', 'runCounter'), 'String',...
            ['Number of Responses: ' num2str(numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses))]);
        set(this.Fig,'XLim', [1 numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).StimTracker(1,:))+1]);
        hAxes = findobj(this.Fig, 'Type', 'axes');
        set(findobj(hAxes,'Type','Line'),'YData', this.MeasurementData.Runs(this.RunTracker.trialcounter).StimTracker(1,:));
        
        set(findobj(this.Fig, 'Type', 'axes'),'YTickLabel', 5:1000)
        %hfig
        return;
end

