function callbackSelection(hObject, ~)
this = get(hObject,'Userdata');
set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'off');
switch get(hObject, 'Tag')
    case 'pbA'
        
        if this.RunTracker.CorrectResponse == 1;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponse = 1;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).Response = 1;
            if this.MeasurementData.Feedback == 1;
                this.updateFeedBack('Correct!');
                set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.7 1 0.7]);
            else
                this.updateFeedBack('');
            end;
            endRun(this);
            return;
        end
        
        if this.RunTracker.CorrectResponse == 2;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponse = 0;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).Response = 1;
            if this.MeasurementData.Feedback == 1;
                this.updateFeedBack('Try again...');
                set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [1 0.7 0.7]);
            else
                this.updateFeedBack('');
            end;
            endRun(this);
            return;
        end
        
    case 'pbB'
        
        if this.RunTracker.CorrectResponse == 1;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponse = 0;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).Response = 2;
            if this.MeasurementData.Feedback == 1;
                this.updateFeedBack('Try again...');
                set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [1 0.7 0.7]);
            else
                this.updateFeedBack('');
            end;
            endRun(this);
            return;
        end
        
        if this.RunTracker.CorrectResponse == 2;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponse = 1;
            this.MeasurementData.Runs(this.RunTracker.trialcounter).Response = 2;
            if this.MeasurementData.Feedback == 1;
                this.updateFeedBack('Correct!');
                set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [0.7 1 0.7]);
            else
                this.updateFeedBack('');
            end;
            endRun(this);
            return;
        end
        
end
