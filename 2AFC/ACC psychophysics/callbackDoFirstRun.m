function callbackDoFirstRun(hObject, ~)
this = get(hObject,'Userdata');
this.updateFeedBack(['']);
set(findobj(this.HContainer, 'Tag', 'startRun'), 'Visible', 'off');
set(findobj(this.HContainer, 'Tag', 'pbA'), 'Visible', 'on');
set(findobj(this.HContainer, 'Tag', 'pbB'), 'Visible', 'on');
doRun(this);
end

