function [sequence, cA_Time, s] = StreamStim(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 48000); %sampling frequency
s = ef(s, 'A_ToneDuration', 0.05); %duration in seconds
s = ef(s, 'A_Amplitude', 0.8); %between -1 and 1
s = ef(s, 'A_CarrierFrequency', 1000); %frequency in Hz
s = ef(s, 'A_ModulationFrequency', 10); %frequency in Hz
s = ef(s, 'A_ModulationPhase', 0); %frequency in Hz
s = ef(s, 'A_ModulationIndex', 0); %frequency in Hz
s = ef(s, 'A_RiseFallTime', 0.01);

s = ef(s, 'B_ToneDuration', 0.05); %duration in seconds
s = ef(s, 'B_Amplitude', 0.8); %between -1 and 1
s = ef(s, 'B_CarrierFrequency', 1000); %frequency in Hz
s = ef(s, 'B_ModulationFrequency', 10); %frequency in Hz
s = ef(s, 'B_ModulationPhase', 0); %frequency in Hz
s = ef(s, 'B_ModulationIndex', 0); %frequency in Hz
s = ef(s, 'B_RiseFallTime', 0.01);

s = ef(s, 'OTO', 0.44);%29
s = ef(s, 'Jitter', 10);%11
value = [];
cA_Time = [];
sequence = [];

%%MAKE 'A' TONE
cA_CarrierFrequency = s.A_CarrierFrequency;
cA_ModulationFrequency = s.A_ModulationFrequency;
cA_Time = (0:s.Fs*s.A_ToneDuration-1)'*1/s.Fs;
cA_Amplitude = s.A_Amplitude*(1 - s.A_ModulationIndex*...
    cos(2*pi*cA_ModulationFrequency*cA_Time + ...
    s.A_ModulationPhase));
cA_Carrier = sin(2*pi*cA_CarrierFrequency*(cA_Time));
cA_tone = cA_Amplitude.*cA_Carrier;
if s.A_RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.A_ToneDuration,'RiseFallTime', s.A_RiseFallTime);
    cA_tone = cWindow(1:numel(cA_tone)).*cA_tone;
else
end
clear cA_Amplitude cA_Carrier cA_CarrierFrequency cA_ModulationFrequency cA_Time cWindow

%%MAKE 'B' TONE
cB_CarrierFrequency = s.B_CarrierFrequency;
cB_ModulationFrequency = s.B_ModulationFrequency;
cB_Time = (0:s.Fs*s.B_ToneDuration-1)'*1/s.Fs;
cB_Amplitude = s.B_Amplitude*(1 - s.B_ModulationIndex*...
    cos(2*pi*cB_ModulationFrequency*cB_Time + ...
    s.B_ModulationPhase));
cB_Carrier = sin(2*pi*cB_CarrierFrequency*(cB_Time));
cB_tone = cB_Amplitude.*cB_Carrier;
if s.B_RiseFallTime > 0
    cWindow = riseFallWindow('Fs', s.Fs, ...
        'Duration', s.B_ToneDuration,'RiseFallTime', s.B_RiseFallTime);
    cB_tone = cWindow(1:numel(cB_tone)).*cB_tone;
else
    
end

clear cB_Amplitude cB_Carrier cB_CarrierFrequency cB_ModulationFrequency cB_Time cWindow

cA_STREAM = [];

cB_OTO_Time = (0:s.Fs*s.OTO-1)'*1/s.Fs;
cB_OTO = zeros(numel(cB_OTO_Time),1);
cB_buff_Time = (0:s.Fs*0.025-1)'*1/s.Fs;
cB_buff = zeros(numel(cB_buff_Time),1);
cB_STREAM = vertcat(cB_buff,cB_OTO);

for x = 1:24
    randy = randi([-s.Jitter s.Jitter]);
    ran1 = (s.OTO/2)+(randy/100);
    ran2 = (s.OTO/2)-(randy/100);
    cA_OTO_Time1 = (0:s.Fs*ran1-1)'*1/s.Fs;
    cA_OTO1 = zeros(numel(cA_OTO_Time1),1);
    cA_OTO_Time2 = (0:s.Fs*ran2-1)'*1/s.Fs;
    cA_OTO2 = zeros(numel(cA_OTO_Time2),1);
    test(x) = numel(vertcat(cA_OTO1,cA_tone,cA_OTO2));
    cA_STREAM = vertcat(cA_STREAM,cA_OTO1,cA_tone,cA_OTO2);
    cB_STREAM = vertcat(cB_STREAM,cB_tone,cB_OTO);
end
if numel(cA_STREAM)>numel(cB_STREAM)
    cEndPad = numel(cA_STREAM)-numel(cB_STREAM);
    cEndPAd = zeros(cEndPad,1);
    cB_STREAM = vertcat(cB_STREAM,cEndPAd);
else
    cEndPad = numel(cB_STREAM)-numel(cA_STREAM);
    cEndPAd = zeros(cEndPad,1);
    cA_STREAM = vertcat(cA_STREAM,cEndPAd);
end
sequence = cA_STREAM+cB_STREAM;
plot(sequence)
sound(sequence,s.Fs)
end
