function callbackSelection(hObject, ~)
this = get(hObject,'Userdata');
set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'off');
set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'off');
cTrial = this.RunTracker.trialcounter;
cRun = this.RunTracker.runcounter;
switch get(hObject, 'Tag')
    case 'pbA'
        this.MeasurementData.Conditions(cTrial).Responses(cRun) = 1;
        if this.RunTracker.CorrectResponse == 1;
            correctResponse(this,'pbA',cTrial,cRun)
        elseif this.RunTracker.CorrectResponse == 2;
            incorrectResponse(this,'pbA',cTrial,cRun)
        end  
    case 'pbB'
        this.MeasurementData.Conditions(cTrial).Responses(cRun) = 2;
        if this.RunTracker.CorrectResponse == 1;
            incorrectResponse(this,'pbB',cTrial,cRun)
        elseif this.RunTracker.CorrectResponse == 2;
            correctResponse(this,'pbB',cTrial,cRun)
        end     
end
end

function correctResponse(this,cInterval,cTrial,cRun)
this.MeasurementData.Conditions(cTrial).CodedResponses(cRun) = 1;
this.updateFeedBack('Correct!');
set(findobj(this.HContainer, 'Tag', cInterval), 'BackgroundColor', [0.7 1 0.7]);
adaptiveUpdate(this);
end

function incorrectResponse(this,cInterval,cTrial,cRun)
this.MeasurementData.Conditions(cTrial).CodedResponses(cRun) = 0;
if this.RunTracker.Step == 1; %Increment counter if wrong at max stepsize
    this.RunTracker.IncorrectAtMax = this.RunTracker.IncorrectAtMax+1;
end;
this.updateFeedBack('Try again...');
set(findobj(this.HContainer, 'Tag', cInterval), 'BackgroundColor', [1 0.7 0.7]);
adaptiveUpdate(this);
end