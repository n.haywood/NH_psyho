function [ReferenceStim, TargetStim] = stimMaker(this)
Fs = this.MeasurementData.ExptParams.Fs;
cCurrentParams = this.MeasurementData.Conditions(this.RunTracker.trialcounter);
cStep = cCurrentParams.StepValues(this.RunTracker.Step);

[ReferenceStim(:,1),cR1] = GenerateStim(...
    'Fs',Fs,...
    'CarrierPhase', 0,...
    'CarrierFrequency', cCurrentParams.CarrierFreq);

[ReferenceStim(:,2),cR2] = GenerateStim(...
    'Fs',Fs,...
    'CarrierPhase', 0,...
    'CarrierFrequency', cCurrentParams.CarrierFreq);

[TargetStim(:,1),cT1] = GenerateStim(...
    'Fs',Fs,...
    'CarrierPhase', 0,...
    'CarrierFrequency', cCurrentParams.CarrierFreq);


[TargetStim(:,2),cT2] = GenerateStim(...
    'Fs',Fs,...
    'CarrierPhase', deg2rad(cStep),...
    'CarrierFrequency', cCurrentParams.CarrierFreq);

set(findobj(this.HTrackContainer, 'Tag', 'C1TextBox'),...
    'String',strrep(struct2str(cT1),' ',''));
set(findobj(this.HTrackContainer, 'Tag', 'C2TextBox'),...
    'String',strrep(struct2str(cT2),' ',''));
set(findobj(this.HTrackContainer, 'Tag', 'C1REFTextBox'),...
    'String',strrep(struct2str(cR1),' ',''));
set(findobj(this.HTrackContainer, 'Tag', 'C2REFTextBox'),...
    'String',strrep(struct2str(cR2),' ',''));
end

function [value,s] = GenerateStim(varargin)
s = parseparameters(varargin{:});
s = ef(s, 'Fs', 44100); %sampling frequency
s = ef(s, 'Duration', 1); %duration in seconds
s = ef(s, 'Amplitude', 1); %between -1 and 1
s = ef(s, 'CarrierPhase', 0); % phase in rad
s = ef(s, 'CarrierFrequency', 500); %frequency in Hz
s = ef(s, 'RiseFallTime', 0.01); %window rise time
value = [];
time = [];

cCarrierFrequency = s.CarrierFrequency;
cAmplitude = s.Amplitude;
time = (0:s.Fs*s.Duration-1)'*1/s.Fs;
cCarrier = sin(2*pi*cCarrierFrequency*(time) + s.CarrierPhase);
cSignal = cAmplitude.*cCarrier;
%% normalize sinusoidal to prevent amplitudes higher than 1 when modulating
cSignal = s.Amplitude*cSignal/max(abs(cSignal));
%% generates rise fall window
cWindow = riseFallWindow('Fs', s.Fs, ...
    'Duration', s.Duration, ...
    'RiseFallTime', s.RiseFallTime);
value = cWindow.*cSignal;
end

