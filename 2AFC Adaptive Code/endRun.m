function endRun(this)
cTrial = this.RunTracker.trialcounter;
cRun = this.RunTracker.runcounter;
cAdapt = this.MeasurementData.ApadtiveParms;
this.MeasurementData.Conditions(cTrial).CompletedFlag = 1;
this.MeasurementData.Conditions(cTrial).RunTime = toc;
if this.RunTracker.IncorrectAtMax == this.MeasurementData.ApadtiveParms.IncorrectAtMaxStepToStop;
    this.MeasurementData.Conditions(cTrial).Threshold...
        = this.MeasurementData.Conditions(cTrial).StepValues(1);
    this.MeasurementData.Conditions(cTrial).StdDev = 0;
elseif this.RunTracker.IncorrectAtMax < cAdapt.IncorrectAtMaxStepToStop;
    for i = 1:numel(this.MeasurementData.Conditions(cTrial).TurnPoints);
        if this.MeasurementData.Conditions(cTrial).TurnPoints(i) == 1;
            thresholdvalue = this.MeasurementData.Conditions(cTrial).StimTrack;
        end;
    end
    thresholdvalue = thresholdvalue(thresholdvalue~=0);
    this.MeasurementData.Conditions(cTrial).Threshold = ...
        mean(thresholdvalue(cAdapt.StepNumInitial+1:...
        cAdapt.StepNumInitial+cAdapt.StepNumFinal));
    
    this.MeasurementData.Conditions(cTrial).StdDev = ...
        std(thresholdvalue(cAdapt.StepNumInitial+1:...
        cAdapt.StepNumInitial+cAdapt.StepNumFinal));
end
writeResultsToXML(this)
UpdateTrackGUIEndRun(this,cTrial)
if this.RunTracker.trialcounter == numel(this.MeasurementData.Conditions)
    msgbox('The experiment is over, thank you for your time!!')
else
    startRun(this);
end
end

function UpdateTrackGUIEndRun(this,cTrial)
for i = 1:numel(this.MeasurementData.Conditions)
    if isempty(this.MeasurementData.Conditions(i).Threshold) == 0
        cXData(i) = this.MeasurementData.Conditions(i).Threshold;
    else
        cXData(i) = min(this.MeasurementData.Conditions(i).StepValues)-1;
    end
end
set(this.HTrackMeanPlot.Children,'YData', cXData)
end