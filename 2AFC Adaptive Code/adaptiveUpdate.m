function adaptiveUpdate(this)
cTrial = this.RunTracker.trialcounter;
cStep = this.RunTracker.Step;
cRun = this.RunTracker.runcounter;
cAdapt = this.MeasurementData.ApadtiveParms;
% record stim value of current response
this.MeasurementData.Conditions(cTrial).StimTrack(cRun) =...
    this.MeasurementData.Conditions(cTrial).StepValues(cStep);
% record step value of current response
this.MeasurementData.Conditions(cTrial).StepTrack(cRun) =...
    this.RunTracker.Step;

if this.RunTracker.IncorrectAtMax == this.MeasurementData.ApadtiveParms.IncorrectAtMaxStepToStop;
    endRun(this)
    return;
end

[change,turnpointTrack] = adaptive_track_engine(this.MeasurementData.Conditions(cTrial).CodedResponses,...
    [cAdapt.StepSizeInitial cAdapt.StepSizeFinal],...
    [cAdapt.StepNumInitial cAdapt.StepNumFinal],...
    [cAdapt.NumCorrectForStep cAdapt.NumIncorrectForStep]);
% Flag to finish run if all turnpoints  are met
if isempty(turnpointTrack) == 0;
    this.MeasurementData.Conditions(cTrial).TurnPoints = turnpointTrack;
    endRun(this)
    return;
end
this.RunTracker.Step = this.RunTracker.Step - change;   % Calculate step change
if this.RunTracker.Step < 1; this.RunTracker.Step = 1; end;
if this.RunTracker.Step > numel(this.MeasurementData.Conditions(cTrial).StepValues);
    this.RunTracker.Step = numel(this.MeasurementData.Conditions(cTrial).StepValues);
end;
set(findobj(this.HTrackContainer, 'Tag', 'currentStep'),...
    'String',['Step: ',num2str(this.MeasurementData.Conditions(cTrial).StepValues(this.RunTracker.Step))]);
writeResultsToXML(this)
cTrackPlot = findobj(this.HTrackPlot);
UpdateTrackGUI(this,cTrial)
doRun(this);
end

function UpdateTrackGUI(this,cTrial)
cYData = this.MeasurementData.Conditions(cTrial).StimTrack;
set(this.HTrackPlot,'XLim', [0 this.RunTracker.runcounter+1]);
set(this.HTrackPlot.Children,'XData', [1:this.RunTracker.runcounter])
set(this.HTrackPlot.Children,'YData', cYData)
end