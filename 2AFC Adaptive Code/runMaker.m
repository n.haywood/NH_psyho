function [ConditionList] = runMaker(this)
%RUNMAKER Summary of this function goes here

cConditions(1).Description = '200 Hz';
cConditions(1).CarrierFreq = 200;

cConditions(2).Description = '500 Hz';
cConditions(2).CarrierFreq = 500;

for i = 1:numel(cConditions);                                              %Use this loop if all condtions share the same params (likely)
    cConditions(i).StimFile = 'aaa';
    cConditions(i).StepValues = [30:-1:0];
    cConditions(i).MaxStep = numel(cConditions(i).StepValues);
    cConditions(i).ConditionNumber = i;
    cConditions(i).CodedResponses =[];
    cConditions(i).StimTracker =[];
    cConditions(i).Threshold =[];
    cConditions(i).StdDev =[];
    cConditions(i).CompletedFlag = 0;
end;
if isempty(this.MeasurementData.ExptParams.ConOverwrite) == 0;
    cConditions = overwriteConditions(this,cConditions);
end
cConditions = setShuffleAndRepeats(this,cConditions);
ConditionList = cConditions;
end

function [cTempConditions] = overwriteConditions(this,cConditions)
for i = 1:numel(this.MeasurementData.ExptParams.ConOverwrite)
    cTempConditions(i) =...
        cConditions(this.MeasurementData.ExptParams.ConOverwrite(i));
end;
end

function [cConditions] = setShuffleAndRepeats(this,cTempConditions)

if  this.MeasurementData.ExptParams.Shuffle == 1; %randomised order
    for i=1:this.MeasurementData.ExptParams.Repeats;
        cRunLinst(i*length(cTempConditions)-...
            length(cTempConditions)+1:...
            i*length(cTempConditions),1)...
            = cTempConditions(:,randperm(size(cTempConditions,2)));
    end
else
    for i=1:this.MeasurementData.ExptParams.Repeats;
        cRunLinst(i*length(cTempConditions)-...
            length(cTempConditions)+1:...
            i*length(cTempConditions),1)...
            = cTempConditions;
    end
end
for i = 1:numel(cRunLinst)
  cRunLinst(i).RunNumber = i;
end;

cConditions = cRunLinst;
end




