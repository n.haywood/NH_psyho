function startRun(this)
this.RunTracker.trialcounter = this.RunTracker.trialcounter+1;
this.RunTracker.Step = 1;
this.RunTracker.runcounter = 0;
this.RunTracker.IncorrectAtMax = 0;
refreshGUI(this,this.RunTracker.trialcounter)
refreshTrackGUI(this,this.RunTracker.trialcounter)
this.RunTracker.inputstate = 1;
set(this.HContainer, 'KeyPressFcn', @this.keyPress);
end

function refreshGUI(this,cTrial)
if cTrial == 1;
    this.updateFeedBack(['Welcome! Press space bar to begin run 1']);
elseif cTrial > 1;
    this.updateFeedBack(['Run complete! Press space bar to begin run ' num2str(cTrial)]);
end;
end

function refreshTrackGUI(this,cTrial)
set(findobj(this.HTrackContainer, 'Tag', 'trialNum'),...
    'String',['Trial Number: ',num2str(cTrial)]);
set(findobj(this.HTrackContainer, 'Tag', 'trialDescription'),...
    'String', ['Trial: ',' ',this.MeasurementData.Conditions(cTrial).Description]);
set(findobj(this.HTrackContainer, 'Tag', 'runCounter'),...
    'String',['Completed Runs: ',num2str(this.RunTracker.runcounter)]);
set(findobj(this.HTrackContainer, 'Tag', 'currentStep'),...
    'String',['Step: ',num2str(this.MeasurementData.Conditions(cTrial).StepValues(1))]);

set(this.HTrackPlot,'YLim', [min(this.MeasurementData.Conditions(cTrial).StepValues),...
    max(this.MeasurementData.Conditions(cTrial).StepValues)]);

for i = 1:numel(this.MeasurementData.Conditions);
    if i <  cTrial
        cText{i,1} = ['Con ' num2str(i) ': '...
            this.MeasurementData.Conditions(i).Description '  '...
            num2str(this.MeasurementData.Conditions(i).RunTime)];
    end
    if i == cTrial
        cText{i,1} = ['Con ' num2str(i) ': '...
            this.MeasurementData.Conditions(i).Description '  IN PROGRESS!!'];
    end
    if i > cTrial
        cText{i,1} = ['Con ' num2str(i) ': '...
            this.MeasurementData.Conditions(i).Description '  --'];
    end
end
set(findobj(this.HTrackContainer, 'Tag', 'InfoTextBox'),...
    'String',cText);
end