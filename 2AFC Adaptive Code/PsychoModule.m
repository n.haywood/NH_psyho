classdef PsychoModule < handle
    properties
        HContainer = [];                                                   % Handle of visual object (figure) for particpant window
        HTrackContainer = [];                                              % Handle of visual object (figure) for experimenter window
        HTrackPlot= [];                                                    % Handle of visual object (figure) for experimenter window
        HTrackMeanPlot = [];                                               % Handle of visual object (figure) for experimenter window
        MeasurementData = [];                                              % This structure contains all the important inforation about stimuli, condtions, and performance - this is saved as output
        RunTracker = [];                                                   % This structure is used by the program to track current run, current step size etc... Is not especially interesting
    end
    
    events
    end
    
    properties (GetAccess = private)                                       % I don't know what this means
    end
    
    methods
        function this = PsychoModule(varargin)
            % We take basic inputs and set up the structure to be used in the experiment
            s = parseparameters(varargin{:});
            s = ef(s,'HContainer', []);                                    % Handle to container object
            s = ef(s,'HTrackContainer', []);                               % Handle to container object
            s = ef(s,'SubjectName',[]);
            s = ef(s,'Expt',[]);
            s = ef(s,'Repeats',[]);
            s = ef(s,'Date',datestr(now, 'dd/mmm/yyyy'));
            s = ef(s,'SaveFolder',[]);
            s = ef(s,'Shuffle',[]);
            s = ef(s,'inputstate',1);
            s = ef(s,'StepSizeInitial',[]);
            s = ef(s,'StepSizeFinal',[]);
            s = ef(s,'StepNumInitial',[]);
            s = ef(s,'StepNumFinal',[]);
            s = ef(s,'NumCorrectForStep',[]);
            s = ef(s,'NumIncorrectForStep',[]);
            s = ef(s,'IncorrectAtMaxStepToStop',[]);
            s = ef(s,'Fs',[]);
            s = ef(s,'ShowThres',[]);
            s = ef(s,'ConOverwrite',0);
            
            cExptParams.Fs = s.Fs;
            cExptParams.SubjectName = s.SubjectName;
            cExptParams.Expt = s.Expt;
            cExptParams.Repeats = s.Repeats;
            cExptParams.Shuffle = s.Shuffle;
            cExptParams.ShowThres = s.ShowThres;
            cExptParams.ConOverwrite = s.ConOverwrite;
            cExptParams.Outfile = ...
                horzcat(s.SaveFolder,s.Expt,'-',s.SubjectName,'.xml');
            
            cApadtiveParms.StepSizeInitial = s.StepSizeInitial;
            cApadtiveParms.StepSizeFinal = s.StepSizeFinal;
            cApadtiveParms.StepNumInitial = s.StepNumInitial;
            cApadtiveParms.StepNumFinal = s.StepNumFinal;
            cApadtiveParms.NumCorrectForStep = s.NumCorrectForStep;
            cApadtiveParms.NumIncorrectForStep = s.NumIncorrectForStep;
            cApadtiveParms.IncorrectAtMaxStepToStop = ...
                s.IncorrectAtMaxStepToStop;
            
            this.MeasurementData.ExptParams = cExptParams;
            this.MeasurementData.ApadtiveParms = cApadtiveParms;
            
            this.HContainer = s.HContainer;
            this.HTrackContainer = s.HTrackContainer;
            set(this.HContainer, 'KeyPressFcn', @this.keyPress);
            this.RunTracker.inputstate = s.inputstate;
            this.RunTracker.trialcounter = 0;
            
            
            this.RunTracker.pahandle = initializeSound(this);
            this.MeasurementData.Conditions = runMaker(this);
            if checkForExistingFile(this) == 1;
                close all; return;
            end;
            
            writeResultsToXML(this)                                         % Inital Save of results file
            this.setPsychoModuleGui;                                        % Build window seen by particpant
            setPsychoModuleTrackGUI(this);                                  % Build the fabuolous window seen by the sexy experimenter
            startRun(this)                                                  % Start (the first) run
        end
        
        function keyPress(this, ~, event)
            % This function
            switch event.Key
                case 'space'
                    if this.RunTracker.inputstate == 1;
                        set(this.HContainer, 'KeyPressFcn', '');
                        this.RunTracker.inputstate = 2;
                        tic;
                        doRun(this)
                    end
                    
                case 'a'
                    if this.RunTracker.inputstate == 2;
                        set(this.HContainer, 'KeyPressFcn', '');
                        set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.4 0.4 0.4]);
                        pause(0.1)
                        hbA = findobj(this.HContainer, 'Tag', 'pbA');
                        hgfeval(get(hbA ,'Callback'),hbA);
                    end
                case 'd'
                    if this.RunTracker.inputstate == 2;
                        set(this.HContainer, 'KeyPressFcn', '');
                        set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [0.4 0.4 0.4]);
                        pause(0.1)
                        hbB = findobj(this.HContainer, 'Tag', 'pbB');
                        hgfeval(get(hbB ,'Callback'), hbB);
                    end
                otherwise
            end
        end
        
        function doRun(this, ~, ~)
            set(findobj(this.HTrackContainer, 'Tag', 'runCounter'),...
                'String',['Completed Runs: ',num2str(this.RunTracker.runcounter)]);
            this.RunTracker.runcounter = this.RunTracker.runcounter +1;
            if rand() > 0.5;    % Randomize which of the two intervals contains the target ('correct') stimuli
                [FirstInterval,SecondInterval] = stimMaker(this);
                this.RunTracker.CorrectResponse = 2;
                set(findobj(this.HTrackContainer, 'Tag', 'currentCorrect'), 'String', 'Correct: Interval 2');
            else
                [SecondInterval,FirstInterval] = stimMaker(this);
                this.RunTracker.CorrectResponse = 1;
                set(findobj(this.HTrackContainer, 'Tag', 'currentCorrect'), 'String', 'Correct: Interval 1');
            end;
            
            pause(1.5);
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'BackgroundColor', [0.8 0.8 0.8]);
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'BackgroundColor', [0.8 0.8 0.8]);
            
            this.updateFeedBack('First Interval');
            this.playback(FirstInterval','pbA');
            this.updateFeedBack('Second Interval');
            this.playback(SecondInterval','pbB');
            
            this.updateFeedBack('Which interval contained the target?');
            set(this.HContainer, 'KeyPressFcn', @this.keyPress);
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'on');
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'on');
        end
        
        function playback(this,cSignal,cPB)
            set(findobj(this.HContainer, 'Tag', cPB), 'BackgroundColor', [0.7 0.7 1]);
            PsychPortAudio('FillBuffer', this.RunTracker.pahandle, cSignal);
            PsychPortAudio('Start', this.RunTracker.pahandle,1,0,1);
            pause(length(cSignal)*1/this.MeasurementData.ExptParams.Fs);
            set(findobj(this.HContainer, 'Tag', cPB), 'BackgroundColor', [0.8 0.8 0.8]);
            this.updateFeedBack('');
            pause(1);
        end
        
        function updateFeedBack(this, message)
            set(findobj(this.HContainer, 'Tag', 'txFeedBack'), 'String', message);
        end
        
        function updateExptFeedBack(this, trialNum,trialDescription)
            set(findobj(this.HEContainer, 'Tag', 'trialNum'), 'String', trialNum);
            set(findobj(this.HEContainer, 'Tag', 'trialDescription'), 'String', trialDescription);
            set(findobj(this.HEContainer, 'Tag', 'runCounter'), 'String',...
                ['Number of Responses: ' num2str(numel(this.MeasurementData.Runs(this.RunTracker.trialcounter).CodedResponses))]);
        end
        
        function [AudioHandle] = initializeSound(this) %Basic PPA routine
            InitializePsychSound;
            PsychPortAudio('Close')
            % PsychPortAudio('GetDevices')
            AudioHandle = PsychPortAudio('Open' ,3,1,0,this.MeasurementData.ExptParams.Fs,2);
        end
        
        function setPsychoModuleGui(this)
            setPsychoModuleGUI(this);
            set(findobj(this.HContainer, 'Tag', 'pbA'), 'Enable', 'off');
            set(findobj(this.HContainer, 'Tag', 'pbB'), 'Enable', 'off');
        end
        
        function writeResultsToXML(this) %Save results to XML
            cStructholder.Results = this.MeasurementData;
            cStructholder.Results = rmfield(cStructholder.Results,'Conditions');
            for i = 1:numel(this.MeasurementData.Conditions)
                cStructholder.Results.Conditions{i}...
                    = this.MeasurementData.Conditions(i);
            end;
            struct2xml(cStructholder,this.MeasurementData.ExptParams.Outfile);
        end
        
        function [exitFlag] = checkForExistingFile(this)
            % Create Output file, and check if already exists (option to exit or overwrite)
            %Should add append option in future!!
            exitFlag = 0;
            if exist(this.MeasurementData.ExptParams.Outfile,'file') == 2;
                choice = questdlg(...
                    'The save file already exisits, would you like to:', ...
                    'Warning!', ...
                    'Exit','Overwrite','Overwrite');
                
                switch choice
                    case 'Exit'
                        exitFlag = 1;
                    case 'Overwrite'
                        exitFlag = 0;
                end
            end;
            
        end
        
    end
    
end
