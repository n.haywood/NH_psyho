clear all
data = xml2struct2('C:\Users\mq20157400\Dropbox\Projects\Brian Project_v2\TIMBRE_EXPT_1\RESULTS\PART1_NH.xml');


output = [];
for x = 1:numel(data.results.Runs)
    cTempData = data.results.Runs{x};
    tempy = struct2str(cTempData.Con_Number);
    t = strrep(tempy,'   Text: ','');  t = strrep(t,'''',''); 
    cons(x,1) = str2num(t);
    
    tempy = struct2str(cTempData.Response);
    t = strrep(tempy,'   Text: ','');  t = strrep(t,'''',''); 
    cons(x,2) = str2num(t);

    tempy = struct2str(cTempData.CompletedFlag);
    t = strrep(tempy,'   Text: ','');  t = strrep(t,'''','');     
    cons(x,3) = str2num(t);
end
cConNumber = max(cons(:));
average = zeros(1,cConNumber);
for y = 1:cConNumber
    for i = 1:numel(data.results.Runs);
        if cons(i,1) == y && cons(i,3) == 1 && cons(i,2) == 1;   
        average(y) = average(y)+1;  
        end
    end
end
%clear average
labels2{1,cConNumber} = {'a'}
for i = 1:cConNumber
   temp =  struct2cell(data.results.Runs{i}.Description);
   t = string(temp)
   v =  str2double(struct2array(data.results.Runs{i}.Con_Number));
   labels2{1,v} = t;
end
